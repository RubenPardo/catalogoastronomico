package com.example.catalogoastronomico;

import java.util.Objects;

/**
 *
 * @author (IOT)
 * @version (2020)
 *
 */
public class Planeta extends Astro {

    // atributos --------------------------------------------
    private boolean habitado;
    private Estrella estrella;// estrella la cual el planeta orbita

    // constructor --------------------------------------------
    public Planeta(String nombre, double brillo, double distancia, boolean habitado, Estrella estrella) {
        super(nombre, brillo, distancia);
        this.habitado = habitado;
        this.estrella = estrella;
    }

    // getters y setters --------------------------------------------
    public boolean isHabitado() {
        return habitado;
    }
    public void setHabitado(boolean habitado) {
        this.habitado = habitado;
    }

    public Estrella getEstrella() {
        return estrella;
    }
    public void setEstrella(Estrella estrella) {
        this.estrella = estrella;
    }

    // equals ---------------------------------------------
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Planeta planeta = (Planeta) o;
        return habitado == planeta.habitado &&
                Objects.equals(estrella, planeta.estrella);
    }


    // toString --------------------------------------------
    // objeto-Astro {nombre: tierra, tipo: Planeta, brillo: 0.0, distancia: 0.0, habitado: true, estrella: sol}
    @Override
    public String toString() {
        return "objeto-Astro {" +
                "nombre: "+nombre+
                ", tipo: "+ this.getClass().getSimpleName() +
                ", brillo: " + brillo +
                ", distancia: " + distancia +
                ", habitado: " + habitado +
                ", estrella: " + estrella.nombre +
                '}';
    }
}
