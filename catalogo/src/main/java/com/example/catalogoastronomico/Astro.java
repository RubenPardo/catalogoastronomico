package com.example.catalogoastronomico;

import java.util.Objects;

/**
 *
 * @author (IOT)
 * @version (2020)
 *
 */

public class Astro {

    // atributos ------------------------------------------------------------------
    protected String nombre;
    protected double brillo;
    /**
     * distancia en años luz
     */
    protected double distancia;

    // constructores ------------------------------------------------------------------
    /**
     *  Un constructor general con los parámetros apropiados para inicializar todos los
     *  atributos de objeto.
     */
    public Astro(String nombre, double brillo, double distancia){
            this.nombre = nombre;
            this.brillo = brillo;
            this.distancia = distancia;

    }//()

    /**
     * constructor por defecto que cree un Astro
     * de nombre "Sirius", de tipo "ESTRELLA", brillo -1.42 y distancia 8.7.
     */
    public Astro(){
        this.nombre = "Sirius";
        this.brillo = -1.42;
        this.distancia = 8.7;
    }//()

    // metodos ------------------------------------------------------------------
    /**
     *
     * @param otro:Astro
     * @return T/F, en función de si el astro this
     * está mas distante o no que el astro otro.
     */
    public boolean masDistante(Astro otro){
        return this.distancia > otro.distancia;
    }//()

    /**
     *
     * @param otro:Astro
     * @return 1 si this es más brillante que otro, -1 si this es menos brillante
     * y 0 si ambos astros son igual de brillantes.
     */
    public int masBrillante(Astro otro){
        return Double.compare(this.brillo, otro.brillo);
    }//()

    /**
     * @return String que describa la forma en la que el astro puede ser observado. Dicho string será:
     * "a simple vista", si su brillo es menor que 5
     * "con prismáticos", si su brillo es mayor o igual que 5 y menor que 7
     * "con telescopio", si su brillo es mayor o igual que 7 y menor que 25
     * "con grandes telescopios", si su brillo es mayor o igual que 25
     */
    public String visibleCon(){
        String resultado = "";
        if(this.brillo < 5){
            resultado = "a simple vista";
        }else if(this.brillo >= 5 && this.brillo < 7){
            resultado = "con prismáticos";
        }else if(this.brillo >= 7 && this.brillo < 25){
            resultado = "con telescopio";
        }else{
            resultado = "con grandes telescopios";
        }
        return resultado;
    }//()

    // equals ---------------------------------------------------------------
    /**
     *
     * @param o objeto a comparar
     * @return True solo si todos los atributos son iguales
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Astro astro = (Astro) o;
        return Double.compare(astro.brillo, brillo) == 0 &&
                Double.compare(astro.distancia, distancia) == 0 &&
                Objects.equals(nombre, astro.nombre);
    }//()

    // getters setters ---------------------------------------------------------
    public double getBrillo() {
        return brillo;
    }
    public void setBrillo(double brillo) {
        this.brillo = brillo;
    }

    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getDistancia() {
        return distancia;
    }
    public void setDistancia(double distancia) {
        this.distancia = distancia;
    }

    // toString ---------------------------------------------------------

    @Override
    public String toString() {
        return "objeto-Astro {" +
                "nombre: " + nombre  +
                ", brillo: " + brillo +
                ", distancia: " + distancia +
                '}';
    }
}