package com.example.catalogoastronomico;
/**
 *
 * @author (IOT)
 * @version (2020)
 *
 */
public class Galaxia extends Astro {

    public Galaxia(String nombre, double brillo, double distancia) {
        super(nombre, brillo, distancia);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    // toString --------------------------------------------
    //objeto-Astro {nombre: vialactea, tipo: Galaxia, brillo: 0.0, distancia: 1275.0 }
    @Override
    public String toString() {
        return "objeto-Astro {" +
                "nombre: "+nombre+
                ", tipo: "+ this.getClass().getSimpleName() +
                ", brillo: " + brillo +
                ", distancia: " + distancia +
                "}";
    }
}
