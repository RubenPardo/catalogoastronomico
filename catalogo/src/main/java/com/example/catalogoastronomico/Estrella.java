package com.example.catalogoastronomico;

import java.util.Objects;

/**
 *
 * @author (IOT)
 * @version (2020)
 *
 */
public class Estrella extends Astro{

    // atributos --------------------------------------------
    private int planetas;// cuantos planetas le orbitan
    private Galaxia galaxia;// a que galaxia pertenece

    // constructor --------------------------------------------
    public Estrella(String nombre, double brillo, double distancia, int planetas, Galaxia galaxia) {
        super(nombre, brillo, distancia);
        this.planetas = planetas;
        this.galaxia = galaxia;
    }

    // getters setters --------------------------------------------
    public int getPlanetas() {
        return planetas;
    }
    public void setPlanetas(int planetas) {
        this.planetas = planetas;
    }

    public Galaxia getGalaxia() {
        return galaxia;
    }
    public void setGalaxia(Galaxia galaxia) {
        this.galaxia = galaxia;
    }


    // equals


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Estrella estrella = (Estrella) o;
        return planetas == estrella.planetas &&
                Objects.equals(galaxia, estrella.galaxia);
    }

    @Override
    public int hashCode() {
        return Objects.hash(planetas, galaxia);
    }

    // toString --------------------------------------------
    //objeto-Astro {nombre: sol, tipo: Estrella, brillo: 4.0, distancia: 149.6, planetas: 8, galaxia: vialactea}
    @Override
    public String toString() {
        return "objeto-Astro {" +
                "nombre: "+nombre+
                ", tipo: "+ this.getClass().getSimpleName() +
                ", brillo: " + brillo +
                ", distancia: " + distancia +
                ", planetas: " + planetas +
                ", galaxia: " + galaxia.nombre +
                '}';
    }

}
