package com.example.catalogoastronomico;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 *
 * @author (IOT)
 * @version (2020)
 *
 */
public class CatalogoAstronomico implements Astros{

    // atributos ---------------------------------------
    /**
     * lista de todos los astros guardados
     */
    private ArrayList<Astro> catalogo;
    /**
     *  Conjunto de String con los nombres de las galaxias a las
     *  que pertenecen los astros del catalogo, en el que no se
     *  permite repetición. Cuando se añado una galaxia al catalogo,
     *  tambien se debe incluir en este conjunto
     */
    private HashSet<String> galaxias;
    /**
     * representa el número de astros a simple vista que contiene el catálogo
     */
    private int numEstrellasSimpleVista;

    public CatalogoAstronomico(){
        this.catalogo = new ArrayList<Astro>();
        this.galaxias = new HashSet<String>();
        this.numEstrellasSimpleVista = 0;
    }

    // metodos -----------------------------------------------

    /**
     * Devuelve true si el Astro en una posición válida
     * del catálogo es una estrella visible a simple vista
     * @param i posicion del catalogo
     * @return T/F si es una estella visible a simple vista
     */
    public boolean esEstrellaSimpleVista(int i){
        return this.catalogo.get(i).visibleCon().equals("a simple vista");
    }//()

    /**
     * Añade un astro al catálogo (comprueba que no esté repetido)
     * y devuelve true si se ha podido añadir
     * Si tiene astros asignados no añadidos se añadiran de forma recursiva
     * ---Ejemplo si un planeta tiene una estrella  se volvera a llamar
     * a anade pasandole la estrella, se comprobara si esta añadida y si tiene una
     * galaxia, si la tiene se volvera a llamar a anade y se repetira el proceso
     * @param a Astro a añadir
     * @return T/F si se pudo añadir
     */
    public boolean anade(Astro a){
        if(!this.catalogo.contains(a)){

            // no se permiten astros con el mismo nombre
            for (Astro astro: catalogo) {
                if(astro.getNombre().equals(a.nombre)){
                    return false;
                }
            }

            // si no esta repetida la añadimos
            this.catalogo.add(a);


            // si es estrella (ya incluye estrella con tipo)
            if(a instanceof Estrella){
                // si es una estrella a simple vista  aumentamos su contador
                if(esEstrellaSimpleVista(this.catalogo.indexOf(buscaAstro(a.nombre)))){
                    this.numEstrellasSimpleVista++;
                }
                Galaxia gTemp =  ((Estrella) a).getGalaxia();
                // si tiene galaxia asignada
                if(gTemp !=null){
                    this.anade(gTemp);// añadimos la galaxia
                }

            }
            // si es una galaxia
            else if(a instanceof Galaxia){
                // añadir la galaxia al hashset
                this.galaxias.add(a.nombre);
            }else if(a instanceof Planeta){
                // cogemos si estrella
                Estrella esTemp = ((Planeta) a).getEstrella();
                // si tiene una asignada la añadimos
                if(esTemp !=null){
                    this.anade(esTemp);
                }
             }

            return true;
        }else{
            return false;
        }
    } // ()

    /**
     * Elimina el astro con el nombre indicado
     * @param nombre del astro
     */
    public void borrar(String nombre){
        Astro astroABorrar = buscaAstro(nombre);
        if(astroABorrar !=null){
            // si es una estrella y esta simple vista descontar del contador
            if(astroABorrar instanceof Estrella && esEstrellaSimpleVista(this.catalogo.indexOf(astroABorrar))){
                this.numEstrellasSimpleVista--;
            }
            // luego lo borramos
            this.catalogo.remove(this.buscaAstro(nombre));
        }



    }// ()

    /**
     * Busca un astro por su nombre
     * @param nombre a buscar
     * @return Astro | Null encontrado, si no lo encuentra devuelve null
     */
    public Astro buscaAstro(String nombre){
        for (Astro a: this.catalogo) {
            if(a.nombre.equals(nombre)){
                return a;
            }
        }
        return null;
    }// ()

    /**
     * Devuelve el primer Astro del catálogo que es más brillante en magnitud absoluta
     * que un Astro dado (devuelve el primero que cumpla esa condición)
     * @param a  Astro dado | Null
     * @return el primer astro que es mas brillante en magnitud absoluta
     */
    public Astro primeroMasBrillanteQue(Astro a){
        if(!this.catalogo.isEmpty()){
            for (Astro astro: this.catalogo) {
                if(Math.abs(astro.brillo) >= Math.abs(a.brillo)){
                    return astro;
                }
            }
        }
        // en el caso de que este vacio se devuelve null
        return null;

    }// ()

    /**
     * Devuelve una lista de objetos Astro
     * con las estrellas visibles a simple vista que contiene el catálogo
     * @return ArrayList:Astro
     */
    public List<Astro> filtraEstrellasSimpleVista(){
        List<Astro> astorSimpleVista =  new ArrayList<Astro>();

        for (Astro a: this.catalogo) {
            if(a.visibleCon().equals("a simple vista") && a instanceof Estrella){
                astorSimpleVista.add(a);
            }
        }

        return astorSimpleVista;
    }//()

    /**
     * Devuelve el Astro que es más brillante en magnitud
     * absoluta de todos los del catálogo
     * @return
     */
    public Astro masBrillante(){
        if(!this.catalogo.isEmpty()){
            Astro aTemp = this.catalogo.get(0);// cojemos el primero para comparar
            for (Astro a: this.catalogo) {
                // si el astro de pivote brilla menos "a" pasa a ser
                // el astro de pivote
                if(Math.abs(aTemp.brillo) < Math.abs(a.brillo)){
                   aTemp = a;
                }
            }

            return aTemp;

        }else{
            return null;
        }

    }

    // getters -----------------------------------------------
    public ArrayList<Astro> getCatalogo() {
        return catalogo;
    }
    public HashSet<String> getGalaxias() {
        return galaxias;
    }
    public int getNumEstrellasSimpleVista() {
        return numEstrellasSimpleVista;
    }

    // toString() ----------------------------------------
    @Override
    public String toString() {
        StringBuilder resultado = new StringBuilder();

        for (Astro astro : this.catalogo) {
            if(astro instanceof  Estrella){

                resultado.append((astro).toString());

            }else if(astro instanceof Planeta){

                resultado.append((astro).toString());

            }else if(astro instanceof Galaxia){

                resultado.append((astro).toString());

            }

            resultado.append("\n");
        }

        return resultado.toString();
    }
}